package ar.edu.ubp.das.beans;

import java.sql.Date;

public class AsistenciaBean {
	int id;
	String cuil, geolocalizacion, mensaje, urlAdjuntos;
	Date fecha;
	boolean isFinalizada, isCancelada;

	
	public String getUrlAdjuntos() {
		return urlAdjuntos;
	}
	public void setUrlAdjuntos(String urlAdjuntos) {
		this.urlAdjuntos = urlAdjuntos;
	}
	public boolean isFinalizada() {
		return isFinalizada;
	}
	public void setFinalizada(boolean isFinalizada) {
		this.isFinalizada = isFinalizada;
	}
	public boolean isCancelada() {
		return isCancelada;
	}
	public void setCancelada(boolean isCancelada) {
		this.isCancelada = isCancelada;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getCuil() {
		return cuil;
	}
	public void setCuil(String cuil) {
		this.cuil = cuil;
	}
	public String getGeolocalizacion() {
		return geolocalizacion;
	}
	public void setGeolocalizacion(String geolocalizacion) {
		this.geolocalizacion = geolocalizacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
