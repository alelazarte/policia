package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.AsistenciaBean;
import ar.edu.ubp.das.db.Dao;

public class MSAsistenciasDao extends Dao<AsistenciaBean, AsistenciaBean> {

	@Override
	public AsistenciaBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		AsistenciaBean bean = new AsistenciaBean();
		bean.setCuil(result.getString("cuil"));
		bean.setFecha(result.getDate("fecha"));
		bean.setGeolocalizacion(result.getString("geolocalizacion"));
		bean.setId(result.getInt("id"));
		bean.setMensaje(result.getString("mensaje"));
		bean.setCancelada(result.getBoolean("cancelada"));
		bean.setFinalizada(result.getBoolean("finalizada"));
		bean.setUrlAdjuntos(result.getString("url_adjuntos"));
		return bean;
	}

	@Override
	public AsistenciaBean insert(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("solicitar_asistencia(?,?,?,?)");
			this.setParameter(1, bean.getCuil());
			this.setParameter(2, bean.getGeolocalizacion());
			this.setParameter(3, bean.getMensaje());
			this.setParameter(4, bean.getUrlAdjuntos());
			ResultSet result = this.getStatement().executeQuery();
			if(! result.next()) {
				throw new SQLException("Error al solicitar asistencia");
			}
			AsistenciaBean asistencia = new AsistenciaBean();
			asistencia.setId(result.getInt("id"));
			return asistencia;
		}
		finally {
			this.close();
		}
	}

	@Override
	public AsistenciaBean update(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AsistenciaBean delete(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("update asistencias set cancelada = 1 where id = ?");
			this.setParameter(1, bean.getId());
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public List<AsistenciaBean> select(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean valid(AsistenciaBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isFinalizada(int id) throws SQLException {
		try {
			this.connect();
			this.setStatement("select finalizada from asistencias where id = ?");
			this.setParameter(1, id);
			ResultSet result = this.getStatement().executeQuery();
			if(result.next()) {
				return result.getBoolean("finalizada");
			}
			return false;
		}
		finally {
			this.close();
		}
	}
}
