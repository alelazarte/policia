package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.db.Dao;

public class MSMensajesDao extends Dao<MensajeBean, MensajeBean>{

	@Override
	public MensajeBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		MensajeBean bean = new MensajeBean();
		bean.setMensaje(result.getString("mensaje"));
		return bean;
	}

	@Override
	public MensajeBean insert(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("agregar_mensaje_usuario(?,?)");
			this.setParameter(1, bean.getIdAsistencia());
			this.setParameter(2, bean.getMensaje());
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public MensajeBean update(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MensajeBean delete(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MensajeBean> select(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("nuevos_mensajes_entidad(?)");
			this.setParameter(1, bean.getIdAsistencia());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	@Override
	public boolean valid(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}


	public void agregarMensajesUsuario(List<MensajeBean> mensajes) throws SQLException {
		try {
			this.connect();
			for(MensajeBean m: mensajes) {
				this.setProcedure("agregar_mensaje_usuario(?,?)");
				this.setParameter(1, m.getIdAsistencia());
				this.setParameter(2, m.getMensaje());
				this.executeUpdate();	
			}
		}
		finally {
			this.close();
		}
	}
}
