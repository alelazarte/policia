package ar.edu.ubp.das.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class Dao<T,S> implements AutoCloseable {

    private String 	           providerName, connectionString;
    private Connection 		   connection;
    private PreparedStatement  statement;
    
    public Dao() {
		try {
	        Context environment = Context.class.cast((new InitialContext()).lookup("java:comp/env"));	
	        this.providerName = (String)environment.lookup("ProviderName");
	        this.connectionString = (String)environment.lookup("ConnectionString");
		} 
		catch (NamingException ex) {
			System.out.println(ex);
		}
    }

    public abstract T make(ResultSet result) throws SQLException;
    public abstract T insert(S bean) throws SQLException;
    public abstract T update(S bean) throws SQLException;
    public abstract T delete(S bean) throws SQLException;
    public abstract List<T> select(S bean) throws SQLException;
    public abstract boolean valid(S bean) throws SQLException;    
    
    public void connect() throws SQLException {
        try {
            Class.forName(this.providerName);
            this.connection = DriverManager.getConnection(this.connectionString);
            this.connection.setAutoCommit(true);
        }
        catch(ClassNotFoundException ex) {
            throw new SQLException(ex.getMessage());
        }
        catch(SQLException ex) {
            throw new SQLException("Datos del login nulos");
        }
    }
        
    public int executeUpdate() throws SQLException {
    	int rows = 0;
        try {
            this.connection.setAutoCommit(false);
            
            rows = this.statement.executeUpdate();
            
            this.connection.commit();
            
            return rows;
        }
        catch(SQLException ex) {
            this.connection.rollback();
            throw new SQLException(ex.getMessage());
        }
        finally {
            this.connection.setAutoCommit(true);
        }
    }

    public List<T> executeUpdateQuery() throws SQLException {
    	try {
            ResultSet  result;
            List<T> list = new LinkedList<T>();

            this.connection.setAutoCommit(false);
            
            result = this.statement.executeQuery();
            while(result.next()) {
                list.add(this.make(result));
            }
            
            this.connection.commit();
            
            return list;
        }
        catch(SQLException ex) {
            this.connection.rollback();
            throw new SQLException(ex.getMessage());
        }
        finally {
            this.connection.setAutoCommit(true);
        }
    }

    public List<T> executeQuery() throws SQLException {
        List<T> list   = new LinkedList<T>();
        ResultSet  result = this.statement.executeQuery();
        
        while(result.next()) {
            list.add(this.make(result));
        }
        
        return list;
    }

    public void setProcedure(String procedure) throws SQLException {
        this.statement = this.connection.prepareCall("{ CALL " + procedure + " }");
    }
    
    public void setProcedure(String procedure, int resultSetType, int resultSetConcurrency) throws SQLException {
        this.statement = this.connection.prepareCall("{ CALL " + procedure + " }", resultSetType, resultSetConcurrency);
    }     
    
    public void setStatement(String statement) throws SQLException {
        this.statement = this.connection.prepareStatement(statement);
    }    

    public void setStatement(String statement, int resultSetType, int resultSetConcurrency) throws SQLException {
        this.statement = this.connection.prepareStatement(statement, resultSetType, resultSetConcurrency);
    }     

    public PreparedStatement getStatement() {
    	return this.statement;
    }
    
    public void setNull(int paramIndex, int sqlType) throws SQLException {
    	this.statement.setNull(paramIndex, sqlType);
    }

    public void setParameter(int paramIndex, long paramValue) throws SQLException {
    	this.statement.setLong(paramIndex, paramValue);
    }

    public void setParameter(int paramIndex, boolean paramValue) throws SQLException {
    	this.statement.setBoolean(paramIndex, paramValue);
    }

    public void setParameter(int paramIndex, int paramValue) throws SQLException {
    	this.statement.setInt(paramIndex, paramValue);
    }
    
    public void setParameter(int paramIndex, short paramValue) throws SQLException {
    	this.statement.setShort(paramIndex, paramValue);
    }

    public void setParameter(int paramIndex, double paramValue) throws SQLException {
    	this.statement.setDouble(paramIndex, paramValue);
    }
    
    public void setParameter(int paramIndex, float paramValue) throws SQLException {
    	this.statement.setFloat(paramIndex, paramValue);
    }

    public void setParameter(int paramIndex, String paramValue) throws SQLException {
    	this.statement.setString(paramIndex, paramValue);
    }

    public void setParameter(int paramIndex, Date paramValue) throws SQLException {
    	this.statement.setDate(paramIndex, paramValue);
    }

    public void setOutParameter(int paramIndex, int sqlType) throws SQLException {
    	((CallableStatement)this.statement).registerOutParameter(paramIndex, sqlType);
    }
      
    public boolean getBooleanParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getBoolean(paramName);
    }

    public long getLongParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getLong(paramName);
    }
    
    public int getIntParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getInt(paramName);
    }
    
    public short getShortParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getShort(paramName);
    }
    
    public double getDoubleParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getDouble(paramName);
    }
    
    public double getFloatParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getFloat(paramName);
    }

    public String getStringParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getString(paramName);
    }
    
    public Date getDateParam(String paramName) throws SQLException {
    	return ((CallableStatement)this.statement).getDate(paramName);
    }
    
    public long getLongParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getLong(paramIndex);
    }
    
    public int getIntParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getInt(paramIndex);
    }
    
    public short getShortParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getShort(paramIndex);
    }
    
    public double getDoubleParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getDouble(paramIndex);
    }
    
    public double getFloatParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getFloat(paramIndex);
    }

    public String getStringParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getString(paramIndex);
    }
    
    public Date getDateParam(int paramIndex) throws SQLException {
    	return ((CallableStatement)this.statement).getDate(paramIndex);
    }
    
    @Override
	public void close() {
        try {
            if(this.statement != null && !this.statement.isClosed()) {
                this.statement.close();
            }
        }
        catch(SQLException ex) { }
        finally {
            try {
                if(this.connection != null && !this.connection.isClosed()) {
                    this.connection.close();
                }
            }
            catch(SQLException ex) { }
        }
    }

}
