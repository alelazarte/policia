package ar.edu.ubp.das.resources;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ar.edu.ubp.das.beans.AsistenciaBean;
import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.beans.UsuarioBean;
import ar.edu.ubp.das.daos.MSAsistenciasDao;
import ar.edu.ubp.das.daos.MSMensajesDao;
import ar.edu.ubp.das.db.Dao;
import ar.edu.ubp.das.db.DaoFactory;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PoliciaResource {
	
	private boolean validarToken(String usuario, String token) throws SQLException {
		if(usuario == null || token == null)
			return false;
		UsuarioBean bean = new UsuarioBean();
		bean.setUsuario(usuario);
		bean.setToken(token);
		Dao<Void, UsuarioBean> dao = DaoFactory.getDao("Sesiones", "ar.edu.ubp.das");
		return dao.valid(bean);
	}
	
	@POST
	@Path("/solicitarAsistencia")
    public Response solicitar(@HeaderParam("usuario") String usuario, @HeaderParam("token") String token, AsistenciaBean bean) {
		try {
			if(! validarToken(usuario, token)) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			Dao<AsistenciaBean, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.insert(bean)).build();
		}
		catch (SQLException e) {
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/cancelarAsistencia")
	public Response cancelar(@HeaderParam("usuario") String usuario, @HeaderParam("token") String token, AsistenciaBean bean) {
		try {
			if(! validarToken(usuario, token)) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			Dao<AsistenciaBean, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.delete(bean)).build();
		}
		catch (SQLException e) {
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/isAsistenciaFinalizada")
	public Response isAsistenciaFinalizada(@HeaderParam("usuario") String usuario, @HeaderParam("token") String token, AsistenciaBean bean) {
		try {
			System.out.println("validando asistencia finalizada con "+usuario+", "+token+", asistencia "+bean.getId());
			if(! validarToken(usuario, token)) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			MSAsistenciasDao dao = new MSAsistenciasDao();
			bean.setFinalizada(dao.isFinalizada(bean.getId()));
			dao.close();
			return Response.status(Response.Status.OK).entity(bean).build();
		}
		catch (SQLException e) {
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/obtenerToken")
	public Response obtenerToken(@HeaderParam("usuario") String usuario, @HeaderParam("clave") String clave) {
		try {
			UsuarioBean bean = new UsuarioBean();
			bean.setUsuario(usuario);
			bean.setClave(clave);
			System.out.println("obtenerToken(usuario: "+usuario+", clave: "+clave);
			Dao<Void, UsuarioBean> dao = DaoFactory.getDao("Sesiones", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.insert(bean)).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/agregarMensajeUsuario")
    public Response agregarMensajeUsuario(@HeaderParam("usuario") String usuario, @HeaderParam("token") String token, List<MensajeBean> bean) {
		try {
			if(! validarToken(usuario, token)) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			Dao<Void, MensajeBean> dao = DaoFactory.getDao("Mensajes", "ar.edu.ubp.das");
			bean.iterator().forEachRemaining(m -> {
				try {
					dao.insert(m);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return Response.status(Response.Status.OK).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/agregarMensajesUsuario")
    public Response agregarMensajesUsuario(@HeaderParam("usuario") String usuario, @HeaderParam("token") String token, List<MensajeBean> mensajes) {
		try {
			if(! validarToken(usuario, token)) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			MSMensajesDao dao = new MSMensajesDao();
			dao.agregarMensajesUsuario(mensajes);
			dao.close();
			return Response.status(Response.Status.OK).build();
		}
		catch (SQLException e) {
			e.printStackTrace();
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}
	
	@POST
	@Path("/nuevosMensajesEntidad")
	public Response nuevosMensajesEntidad(@HeaderParam("usuario") String usuario, @HeaderParam("token") String token, List<MensajeBean> beans) {
		try {
			if(! validarToken(usuario, token)) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			Dao<MensajeBean, MensajeBean> dao = DaoFactory.getDao("Mensajes", "ar.edu.ubp.das");
			return Response.status(Response.Status.OK).entity(dao.select(beans.get(0))).build();
		}
		catch (SQLException e) {
    		return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
		}
	}

}
